<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\Article;
use App\Like;
use App\Comment;
use phpDocumentor\Reflection\Types\Integer;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function create()
    {
        
        $userId=Auth::user()->id;
        $user=User::find($userId);
        $category=Category::get();
        return view('article.postform',['category'=>$category, 'userData'=>$user]);
    }

    public function store(Request $request)
    {
       
        $article = Article::create([
            "article_type" => $request->post_type,
            "title"=>$request->title,
            "article_body"=>$request->post_body,
            "user_id"=>Auth::user()->id,
        ]);
        
        $article->addMediaFromRequest('image')->toMediaCollection('default');
        $article->save();
        return redirect('/home');
    }
    public function show($id){
     
        $article=Article::find($id);
        $userData=User::get();
        $image=Article::find($id)->getMedia('default');
        $comment=Comment::where('post_id',$id)->simplePaginate(3);
        
        $like=Like::where('user_id',Auth::user()->id)->where('post_id',$id)->count();
        
        return view('article.post',['like'=>$like,'userData'=>$userData,'postData'=>$article,'image'=>$image,'comments'=>$comment]);
    }
    public function edit($id)
    {
        $article=Article::find($id);
        $userData=User::get();
        $image=Article::find($id)->getMedia('default');
        $category=Category::get();
        return view('article.postEdit',['category'=>$category,'userData'=>$userData,'postData'=>$article,'image'=>$image]);
    }
    public function update(Request $request)
    {
    
        $article=Article::find($request->id);
        $article->title=$request->title ? $request->title:$article->title;
        $article->article_type=$request->article_type ? $request->article_type:$article->article_type;
        $article->article_body=$request->article_body ? $request->article_body:$article->article_body;
        $article->update();
        if (!empty($request->image)) {
            $art=$article->getMedia();
            $art[0]->delete();
            $article->addMediaFromRequest('image')->toMediaCollection('default');
        } 
        
        
    
        
        return redirect('/myposts');
    }
    public function delete($id)
    {
     
        $article=Article::find($id);
        if ($article->user_id==Auth::user()->id) {
            $article->delete();
        }
        
        return redirect()->back();
    }
    public function mypost()
    {
        $user=Auth::user()->id;
        $allPosts=Article::where('user_id',$user)->simplePaginate(4);
        $likes=Like::where('user_id',Auth::user()->id)->get();
        foreach($allPosts as $post){
            $post->getMedia();
            $post->isLiked = $likes->where('post_id',$post->id)->count();
        }

        return view('article.postshow',['posts'=>$allPosts]);
    }

    public function allArticle()
    {
        
        $allPosts=Article::with('user')->simplePaginate(4);
        $likes=Like::where('user_id',Auth::user()->id)->get();
        foreach($allPosts as $post){
            $post->getMedia();
            $post->isLiked = $likes->where('post_id',$post->id)->count();
        }
        return view('article.allarticles',['posts'=>$allPosts]);
    }
    public function sectionArticle($id)
    {
        $allPosts=Article::with('user')->where('article_type',$id)->simplePaginate(5);
        $catagory=Category::find($id);
        $likes=Like::where('user_id',Auth::user()->id)->get();
        foreach($allPosts as $post){
            $post->getMedia();
            $post->isLiked = $likes->where('post_id',$post->id)->count();
        }
    
        return view('article.allarticles',['posts'=>$allPosts,'catagory'=>$catagory]);
        
    }
    


    public function likes(Request $request )
    {
        
        $likes= new Like;
        $likes->user_id=Auth::user()->id;
        $likes->post_id=$request->post_id;
        $likes->save();
        $likeCount=Article::find($request->post_id);
        $likeCount->like_count=(int)$likeCount->like_count+1;
        $likeCount->update();
        return redirect('/post'.'/'.$request->post_id);
        
    }

    public function dislikes(Request $request )
    {
        
        $like=Like::get()->where('user_id',Auth::user()->id);
        
        foreach ($like as $like) {
            if ($like->post_id==$request->post_id) {
                $like->delete();
            }
        }
        $likeCount=Article::find($request->post_id);
        $likeCount->like_count=(int)$likeCount->like_count-1;
        $likeCount->update();

        return redirect('/post'.'/'.$request->post_id);
        
    }
    public function homeArticle()
    {
       $article=Article::order_by('datetime', 'desc')->limit(4);
    }

    public function comment(Request $request)
    {
        $userId=Auth::user()->id;
        $comment=new Comment;
        $comment->comment=$request->comment;
        $comment->user_id=$userId;
        $comment->post_id=$request->post_id;
        $comment->save();
        return redirect()->back();
        
    }



}
