<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Gender;
use App\BloodGroup;
use App\User;
use App\Article;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles=Article::latest('created_at')->take(4)->get();
        $userData=User::get();
        foreach ($articles as $article) {
            $article->getMedia();
        }

        $popularArticles = Article::orderByDesc('like_count')->take(3)->get();
        foreach ($popularArticles as $article) {
            $article->getMedia();
        }
      
        return view('home',['articles'=>$articles,'popularArticles'=>$popularArticles, 'userData'=>$userData]);
    }
    public function profile()
    {
        $userData=auth()->user();
        $bloods=BloodGroup::get();
        $articles=Article::where('user_id',auth()->user()->id)->latest('created_at')->take(4)->get();
        foreach ($articles as $article) {
            $article->getMedia();
        }
  
        return view('profile',['userData'=>$userData,'articles'=>$articles,'bloods'=>$bloods]);
    }

    public function create()
    {
        $userData=auth()->user();
        
        $gender=Gender::get();
        $bloods=BloodGroup::get();
        
        return view('profileForm',['userData'=>$userData,'gender'=>$gender,'bloods'=>$bloods]);
    }
    public function update( Request $request)
    {
       $user=User::find(Auth::user()->id);
       $user->name=$request->name ? $request->name:$user->name;
       $user->email=$request->email ? $request->email:$user->email;
       $user->address=$request->address ? $request->address:$user->address;
       $user->Gender=$request->Gender ? $request->Gender:$user->Gender;
       $user->bloodgroup_id=$request->bloodgroup_id ? $request->bloodgroup_id:$user->bloodgroup_id;
       $user->update();

        
        return redirect('/profile');
    }

    public function imageShow()
    {
         $img=Article::find(Auth::user()->id)->getMedia('default');
        
        return view('image', ['images'=>$img]);
    }

    public function imageSave(Request $request)
    {
        $userId=Auth::user()->id;
        $userData=User::find($userId);
        $userData->addMediaFromRequest('image')->toMediaCollection();
        return redirect()->back();
    }

}
