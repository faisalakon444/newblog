<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model implements HasMedia
{  
    use HasMediaTrait;
    use SoftDeletes;
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function catagory()
    {
        return $this->belongsTo('App\Catagory');
    }
     public function article()
    {
         return $this->hasMany('App\Comment');
    }

    protected $fillable = ['article_type','title','article_body','user_id'];
}
