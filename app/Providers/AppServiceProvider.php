<?php

namespace App\Providers;

use App\Article;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Composer;

use App\Category;
use PhpParser\Node\Stmt\Foreach_;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        
        View::composer('includs.menu',function($view){
            $sections = Category::get();
            $view->with('sections',$sections);
        });
        View::composer('includs.side',function($view){
            $articles= Article::get();
            $sections = Category::get();
            foreach($sections as $section){
                $section->articleCount = $articles->where('article_type',$section->id)->count();
            }
            $view->with('sections',$sections,'article',$articles);
        });
            
        
    }
}
