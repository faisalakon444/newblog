<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('/profile-update', 'HomeController@create')->name('profile.form');
Route::post('/profile-update', 'HomeController@update')->name('profile.update');
Route::get('/postform', 'ArticleController@create')->name('post.form');
Route::post('/postsave', 'ArticleController@store')->name('post.save');
Route::get('/myposts', 'ArticleController@mypost')->name('post.show');
// Route::post('/post', 'ArticleController@post')->name('post.index');
Route::post('/likes', 'ArticleController@likes')->name('post.like');
Route::post('/dislikes', 'ArticleController@dislikes')->name('post.like');
Route::get('/post/{id}', 'ArticleController@show')->name('post.index');
Route::post('/comment', 'ArticleController@comment')->name('post.comment');

Route::get('/post-edit/{id}', 'ArticleController@edit')->name('post.edit');
Route::get('/post-delete/{id}', 'ArticleController@delete')->name('post.delete');

Route::post('/post-update', 'ArticleController@update')->name('post.update');

Route::get('/allpost', 'ArticleController@allArticle')->name('articles');
Route::get('/section/{id}', 'ArticleController@sectionArticle')->name('section.articles');

Route::get('/about', function () {
    return view('about');
});

Route::get('/image','HomeController@imageShow');
Route::post('/image/upload','HomeController@imageSave');

