-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 05, 2020 at 08:57 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `article_type` tinyint(4) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `like_count` int(11) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `article_type`, `title`, `article_body`, `created_at`, `updated_at`, `like_count`, `deleted_at`) VALUES
(51, 1, 1, 'Gravity-defying ski moguls and snowmaking in warmish weather', 'Last week I was at the Grandvalira ski area in Andorra, having a great time whizzing down the slopes in glorious sunshine. Sitting on the chairlifts, my mind naturally turned to the physics involved in maintaining the pistes – and I became fascinated by two things: the moguls that quickly form on an initially smooth slope; and the snowmaking systems that help keep the slopes full of snow.\r\n\r\n\r\nNights are very busy at a ski area. Snow cannons hiss as they take advantage of the colder night-time air while huge bulldozer-like “piste bashers” smooth-out the slopes for the next day’s skiing.\r\n\r\nAfter a few hours of being skied on, however, a smooth slope can be covered in moguls – bumps that are about a metre or so across and tens of centimetres high. These tend to form on steeper slopes where skiers control their speed by weaving back and forth across the piste, rather than plunging straight down. Each turn pushes some snow up into a pile and it is easier for successive skiers to weave around these piles, further building them up into a mogul field.', '2020-03-18 23:14:09', '2020-03-22 01:26:29', 6, NULL),
(52, 1, 1, 'Gravity-defying ski moguls and snowmaking in warmish weather', 'I’ll complete my explanation of how physicists know that all elementary particles (such as the electron) are identical by delving into quantum mechanics, a fascinating area of physics that was discovered and developed over the first 3 decades of the 20th century (1900-1930). It should be entirely possible to read Part 2 without reading Part 1; even though they both have to do with why particles are identical, both are self-contained and neither is dependent on the other. Part 1 is basically the explanation as could have been understood circa 1900, while Part 2 is the explanation as understood by 1930 — after quantum mechanics had been completed.\r\nIn classical statistical mechanics, you can represent the different possibilities for the state of a system by probabilities. For example, if you know the temperature and pressure of a gas, there is a statistical distribution (called a “probability density function”) of the different particles making up the gas. These particles are bouncing around randomly. At a high temperature, you’re more likely to find an individual molecule of the gas moving rapidly; at a low temperature, you’re more likely to find an individual molecule of the gas moving slowly. But either way there is a whole range of possibilities.\r\nIn quantum mechanics, the same is true but it gets a little more complicated. The probability density function in quantum mechanics is given by the square of the magnitude of a complex function called the “wave function”. By complex I mean instead of a function of real numbers (x = 1, 2, 3.4, 9.8, etc.), it’s a function of complex numbers, each of which has a real and an imaginary part (z = 1+i, 2+3.5i, 4.8+9i, etc.) If you’ve never encountered this before, I’m sure it sounds really weird. But I can’t say much more other than: this is just how quantum mechanics works — it’s kinda weird!', '2020-03-18 23:19:44', '2020-03-22 01:26:47', 1, NULL),
(53, 1, 1, 'How do we know that all electrons are identical?', 'You may have heard physicists say “all electrons are identical”. But what does that mean? How could we ever know that, if they are so small that nobody has ever even seen one directly? And even if we do have some way of knowing that, why does it matter?\r\nWhen it comes to any kind of thing that we can’t see directly, we tend to conceptualize it by analogy with things that we can see directly. For example, we can see cars directly. And we know from experience that even though most (say, for example) 2015 Toyota Camry’s are nearly identical, we also know that there are little differences here and there between the particular instances of them. Even if they were all manufactured by the same process, you might find one that has a minor scratch on the front right bumper, another that has a tiny dent near the left rear wheel, etc. By analogy, it seems natural to assume that since we can’t see electrons well enough to inspect every single one of them carefully, there could be one here or there that has a scratch or a dent. Even if most of them are nearly identical, it sounds like a pretty arrogant and far fetched thing for a physicist to claim that all of them are identical.', '2020-03-18 23:21:45', '2020-03-22 01:26:43', 2, NULL),
(54, 1, 2, 'The Arrival of Cobots to Industry Results in Significant Job Creation', 'Most manufacturers say that they aren’t planning to use collaborative robots to replace humans. Rather, they want humans to work with cobots to increase their own productivity. Currently, humans do a lot of repetitive tasks. A human worker could program the cobot to complete these dull tasks. As a result, the human can do more satisfying work for both them and the employer.\r\n\r\nEven when cobots replace humans for particular tasks, there are often jobs that the cobots can’t do. These roles often require creative, critical thinking. By having a collaborative robot perform dirty, dull, and dangerous tasks, the company can better use their human workers to help grow their company by developing new products and processes.\r\n\r\nYour employer wants you to be happy. Surprised to hear that? Well, it’s true. Happier, engaged employees are more productive and are great champions for a brand. Companies often invest a lot into their employees to keep them on board due to the costs of retraining a new employee. Cobots are part of a strategy to help employees enjoy their jobs, not to take them away.', '2020-03-18 23:25:03', '2020-03-22 03:59:55', 2, '2020-03-22 03:59:55'),
(55, 1, 2, 'Construction Businesses Beginning to Value AI and Robotics for Multiple Challenges', 'Construction service robots often have a specific application. For example, robots have been designed to help with masonry tasks like bricklaying and delivering cement blocks, easing a lot of physical strain. The robots perform the work faster and allow workers to focus on more skilled tasks like checking the quality of mortar joints.\r\n\r\nOther robots used on the job site include drones that are equipped with 3D vision and AI. These drones fly around the job and keep track of how much work has been completed. This information is critical to a contractor who may be penalized for missing a deadline or incentivized for finishing the job early while meeting a certain quality standard.\r\n\r\nSome workers get to use supportive robotic vests that lessen fatigue while performing tasks like drilling. Other designs include robots that drive around the job and mark locations of walls and other features, using the blueprints as a source. Want to know something else about robots? They don’t get sore muscles. They don’t need breaks. And they don’t call in sick.\r\n\r\nDiscover how construction robots are revolutionizing their industry by improving conditions for workers and helping businesses to meet their changing demands.', '2020-03-18 23:26:33', '2020-03-19 00:17:50', 1, NULL),
(56, 1, 2, 'What are the Best Ways to Start a Career in the Robotics Industry?', 'Learn the Basics. Even entry-level job-seekers need to be able to understand sophisticated, complex systems. Start with the basics of electronics. Fortunately, you can learn a lot of the basics with online study. Take a course, visit industry websites, and watch the growing number of videos available. Pick up a few books. You can probably find some at your local library.\r\n\r\nPick a Field. Once you have a good handle on the basics, you’ll probably find it easy to pick a field. If you’re not sure, do some networking. Reach out to online communities or find robotics enthusiasts in your area. The amount of formal education you’ll have to pursue often depends on your field and job. Techs often only need a two-year degree while Engineers will need a four-year degree.\r\n\r\nChoose Your Specialty. Within your field, there will be many areas where you can specialize. And there will also be plenty of industries to choose from. Say you want to get into robotic welding. The automotive industry may seem like a no-brainer. But there are also opportunities in aerospace, military, and industrial facilities. Perhaps find an internship first.', '2020-03-18 23:28:38', '2020-03-19 00:17:58', 1, NULL),
(57, 1, 2, 'Hospitality Industry Looking to AI for Improved Service, Security, and More', 'The hospitality industry and AI have come together to provide a truly remarkable guest experience. It might seem impossible, but hotels and others in the travel space hope to offer you the same, and even better, service that you currently receive from the best in the hospitality industry.\r\n\r\nYou’ve most likely interacted with AI in the last year. If you’ve called an automated voice prompt or sent messages to a chatbot, you communicated with AI-powered technology. Or if you’ve used your smartphone’s digital assistant or have a voice assistant at home to lock your doors or turn on your lights, you’ve used AI. What could AI mean for your next hotel stay?', '2020-03-18 23:32:45', '2020-03-19 00:17:56', 1, NULL),
(58, 3, 3, 'black holes bend light around themselves, opening a new possibility for future observations.', 'By now, nearly all of us have seen the Event Horizon Telescope’s iconic image of a black hole. The supermassive black hole sits at the center of the galaxy M87 in the constellation Virgo, happily chomping down on a light diet of hot gas. Photons radiated from this gas and from elsewhere loop around the black hole just outside the event horizon, the infamous point of no return. These photons outline the event horizon’s round silhouette to create the glowing halo and black hole “shadow” shown in the image.\r\n\r\nblack hole shadow M87Scientists used radio observations, computer codes, and months of analysis to construct this image of the supermassive black hole in the center of the galaxy M87. The dark center marks where light plunges past the event horizon, never to return; the crescent is from the light and hot gas just outside.\r\nEHT Collaboration\r\nThe inner edge of the silhouette’s bright halo is called the photon ring. It marks the closest circuit light can make around the black hole without plunging into it. Einstein’s general theory of relativity predicts the ring’s size and shape, and many have hailed the M87 image as a verification of Einstein’s gravity.\r\n\r\nBut it seems we did not understand this ring as well as we thought we did. Calculations by a team at Harvard’s Black Hole Initiative and their colleagues show that for spinning black holes, the photon ring is actually not a single ring. Instead, it’s an infinite series of concentric “subrings,” all piled up together, each one thinner than the one just outside it. Careful observations may enable us to use the subrings as a unique window, shining light (pun intended) on the way the black hole warps the fabric of spacetime around it.', '2020-03-18 23:52:00', '2020-03-19 00:18:17', 1, NULL),
(59, 3, 3, 'The European Space Agency and Roscosmos have postponed the launch of their Mars lander', 'And then there were three. On March 12th, the European Space Agency (ESA) released a statement that the launch of the ExoMars mission, which will include the rover Rosalind Franklin, is postponed from this summer to the next launch window in 2022.\r\n\r\nThe decision comes after evaluation by project teams and the Russian and European Inspectors General, which concluded that more tests of the spacecraft hardware, software, and integration were needed. The slip comes amid the ongoing coronavirus pandemic, which has brought many activities in the European Union to a halt, and may compromise the final phase of ExoMars preparations.\r\n\r\n“We want to make ourselves 100% sure of a successful mission,” says ESA Director General Jan Wörner in a recent press release. “We cannot allow ourselves any margin of error. More verification activities will ensure a safe trip and the best scientific results on Mars.”\r\n\r\nThe mission has faced delays before. Earth reaches an optimal launch window in terms of minimum time and fuel needed to head to Mars about every 26 months. In 2016 ESA delayed the rover from the 2018 to the 2020 launch window, citing difficulties working multiple projects in the ExoMars program in parallel and in a compressed timeline.', '2020-03-18 23:52:55', '2020-03-18 23:53:36', 0, NULL),
(60, 3, 3, 'Cold weather keeping you inside at night?', 'IC 1747 and Czernik 6 : Also known as the Holepunch Nebula, IC 1747 is a 12th-magnitude planetary nebula 13 arcseconds in diameter located ½° southeast of Epsilon Cas and 1′ southeast of a 12.7-magnitude star. A small, circular puff of light caught my eye at 64×, but increasing the magnification to 245× and adding an Ultra High Contrast (UHC) filter transformed it into a thick ring with a slightly darker center. I love digging out the white dwarf stars at the centers of planetaries, but this 15.4-magnitude ember eluded my gaze.\r\n\r\nHigh magnifications work wonders on smaller planetary nebulae, often revealing shapes, central stars, and brighter nodules embedded within their disks. If at first you see little more than a smudge, try 200× or even 400× to sweeten the detail. Don\'t be bashful.\r\n\r\nA little more than ½° southeast of IC 1747 you\'re in for a treat: Czernik 6. At first I thought I saw only a 9th-magnitude star but then I noticed nearly a dozen fainter members huddled in its light. Only 2′ across, this dainty gem is worth the side tour!', '2020-03-18 23:54:56', '2020-03-19 00:04:02', 0, NULL),
(61, 3, 4, 'Introducing Dreamer: Scalable Reinforcement Learning', 'Research into how artificial agents can choose actions to achieve goals is making rapid progress in large part due to the use of reinforcement learning (RL). Model-free approaches to RL, which learn to predict successful actions through trial and error, have enabled DeepMind\'s DQN to play Atari games and AlphaStar to beat world champions at Starcraft II, but require large amounts of environment interaction, limiting their usefulness for real-world scenarios.\r\n\r\nIn contrast, model-based RL approaches additionally learn a simplified model of the environment. This world model lets the agent predict the outcomes of potential action sequences, allowing it to play through hypothetical scenarios to make informed decisions in new situations, thus reducing the trial and error necessary to achieve goals. In the past, it has been challenging to learn accurate world models and leverage them to learn successful behaviors. While recent research, such as our Deep Planning Network (PlaNet), has pushed these boundaries by learning accurate world models from images, model-based approaches have still been held back by ineffective or computationally expensive planning mechanisms, limiting their ability to solve difficult tasks.', '2020-03-18 23:59:46', '2020-03-19 00:03:38', 0, NULL),
(62, 3, 4, 'Turning any 2D photo into 3D using convolutional neural nets', 'Our 3D Photos feature on Facebook launched in 2018 as a new, immersive format for sharing pictures with friends and family. The feature has relied on the dual-lens “portrait mode” capabilities available only in new, higher-end smartphones, however. So it hasn’t been available on typical mobile devices, which have only a single, rear-facing camera. To bring this new visual format to more people, we have used state-of-the-art machine learning techniques to produce 3D photos from virtually any standard 2D picture. This system infers the 3D structure of any image, whether it is a new shot just taken on an Android or iOS device with a standard single camera, or a decades-old image recently uploaded to a phone or laptop.\r\n\r\nThis advance makes 3D photo technology easily accessible for the first time to the many millions of people who use single-lens camera phones or tablets. It also allows everyone to experience decades-old family photos and other treasured images in a new way, by converting them to 3D. People with state-of-the-art dual-camera devices can benefit, too, since they can now use their single, front-facing camera to take 3D selfies. Anyone with an iPhone 7 or higher, or a recent midrange or better Android device, can now try these options in the Facebook app.', '2020-03-19 00:01:44', '2020-03-19 00:01:44', 0, NULL),
(63, 3, 4, 'Pushing the state of the art in 3D content understanding', 'n order to interpret the world around us, AI systems must understand visual scenes in three dimensions. This need extends beyond robotics, navigation, and even augmented reality applications. Even with 2D photos and videos, the scenes and objects depicted are themselves three-dimensional, of course, and truly intelligent content-understanding systems must be able to recognize the geometry of a cup’s handle when it’s being rotated in a video, or identify which objects are in the foreground and background of a photo.\r\n\r\nToday, we’re sharing details on several new Facebook AI research projects that advance the state of the art in 3D image understanding in different but complementary ways. This work, which is being presented at the International Conference on Computer Vision (ICCV) in Seoul, addresses a variety of use cases and circumstances, with different types and amounts of training data and inputs.\r\n\r\nMesh R-CNN is a novel, state-of-the-art method to predict the most accurate 3D shapes in a wide range of real-world 2D images. This method, which leverages our general Mask R-CNN framework for object instance segmentation, can detect even complex objects, such as the legs of a chair or overlapping furniture.\r\n\r\nUsing an alternative and complementary approach to Mesh R-CNN, termed C3DPO, we’re the first to achieve a successful large-scale 3D reconstruction of nonrigid shapes on three', '2020-03-19 00:03:06', '2020-03-19 00:03:06', 0, NULL),
(64, 2, 5, 'Viruses for the Good: Gene Therapy for Sickle Cell Disease', 'While the world worries about novel coronavirus SARS-CoV-2, other viruses continue to be used for the good – as vectors that ferry in healing genes for gene therapy and editing.\r\n\r\nCharles Hough calls himself “reborn” after lentiviruses – disabled versions of HIV – gave his blood cells the gene that overrides the mutant one that causes his red blood cells to sickle.\r\n\r\nSymptoms and Treatments\r\n\r\nSickle cell disease (SCD) is the consequence of the simplest of genetic glitches, yet with wide-ranging effects on the individual, family, and population levels.\r\n\r\n\r\nHemoglobin\r\nIn the disease, a single DNA base substitution switches out a single amino acid in the encoded protein, the beta subunit of hemoglobin. That tiny change, replacing glutamic acid with valine, alters the surfaces of hemoglobin molecules. They glom into curved, cable-like twisted rods that bend the red blood cells that house them into rigid, fragile, sickle shapes.\r\n\r\nThe sickled cells lodge in small diameter blood vessels, blocking circulation, which accelerates the sickling as oxygen falls, like a hurled stone sending ripples onto a pond’s surface. The resulting joint pain feels like having a hand or foot slammed in a door. Bones and intestines ache, as the profound fatigue of anemia sets in while the swollen spleen and liver work overtime to destroy the sickled cells. Infections are common. The name “sickle cell anemia” was changed to “disease” decades ago to include the spectrum of symptoms.', '2020-03-19 00:07:58', '2020-03-19 00:07:58', 0, NULL),
(65, 2, 5, 'New View of the Brink of Cancer May Validate Preventive Mastectomy', 'Women who have prophylactic mastectomies to stay ahead of a BRCA2 mutation may have made a wise choice, according to findings of a study just published in Science Advances. I’m amazed at the bravery of these women who go the Angelina Jolie route.\r\n\r\nInheriting a BRCA2 mutation brings a 50 to 80 percent lifetime risk of developing breast cancer. But how does that population statistic shake out at a personal level? Is an individual among the 20 to 50 percent who won’t develop the cancer? If not, how long can she safely delay surgery until just before the first inklings of cancer arise?\r\n\r\nThere’s no crystal ball that can predict when cancer will begin, but Leif W. Ellisen and colleagues at the Massachusetts General Hospital Cancer Center are coming close. Their clever study detects genetic changes that happen before the effects of the underlying BRCA2 mutation kick in.\r\n\r\n\r\n(NHGRI)\r\nThe BRCA2 risk mutation is stitched into every cell from conception. But it takes further mutations, occurring decades later in the breast cells themselves, to jumpstart and then drive the cancer. A BRCA2 mutation is “germline,” meaning it’s in every cell from conception, whereas the other mutations are “somatic,” starting in body (not sperm or egg) cells.', '2020-03-19 00:10:08', '2020-03-19 00:10:08', 0, NULL),
(66, 2, 6, 'Donald Trump launches Artificial Intelligence Initiative', 'A.I. has become a defining issue of our time, affecting national security, economic development, human rights, social media and many other domains. President Donald Trump signed an executive order launching the “American AI Initiative”, directing federal agencies to officially focus more on artificial intelligence technology. \r\n\r\nThe administration will be assigning federal agencies specific timelines for “deliverables” and expects to release more information about the initiative in the next six months.\r\n\r\nThe initiative is following at least 18 other countries that have announced their own artificial intelligence strategies.\r\n\r\n\r\nU.S. President Donald Trump signed an executive order, launching an initiative to ensure the country\'s leadership in artificial intelligence (AI).\r\n\r\nThe five “key pillars” of the American AI Initiative are:\r\n\r\nResearch and Development\r\n\r\nInfrastructure\r\n\r\nGovernance\r\n\r\nWorkforce\r\n\r\nInternational Engagement\r\n\r\nThe American AI Initiative follows several steps the Trump administration has already taken on A.I.', '2020-03-19 00:11:31', '2020-03-22 13:13:30', 0, NULL),
(67, 2, 6, 'Machine Learning and Why It Matters For The World', 's one of the hottest buzzwords these days, Machine learning is becoming so much of a part of what businesses are pursuing this year. By definition, machine learning is any technology that uses algorithms to try to create repeatable results. When we talk about machine learning, we’re talking about machine learning algorithms, no matter what form they may take.\r\n\r\nThis paper looks to understand some of the context surrounding machine learning and how it can be useful. Read the basics and essential concepts that help define machine learning and its new important role in business as we know it.\r\ns one of the hottest buzzwords these days, Machine learning is becoming so much of a part of what businesses are pursuing this year. By definition, machine learning is any technology that uses algorithms to try to create repeatable results. When we talk about machine learning, we’re talking about machine learning algorithms, no matter what form they may take.\r\n\r\nThis paper looks to understand some of the context surrounding machine learning and how it can be useful. Read the basics and essential concepts that help define machine learning and its new important role in business as we know it.', '2020-03-19 00:15:21', '2020-03-19 00:15:21', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blood_groups`
--

CREATE TABLE `blood_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `groups` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blood_groups`
--

INSERT INTO `blood_groups` (`id`, `groups`, `created_at`, `updated_at`) VALUES
(1, 'A+', NULL, NULL),
(2, 'A-', NULL, NULL),
(3, 'AB+', NULL, NULL),
(4, 'AB-', NULL, NULL),
(5, 'O-', NULL, NULL),
(6, 'O+', NULL, NULL),
(7, 'B+', NULL, NULL),
(8, 'B-', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `catagory_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `catagory_name`, `created_at`, `updated_at`) VALUES
(1, 'Physics', NULL, NULL),
(2, 'Robotics', NULL, NULL),
(3, 'Astronomy', NULL, NULL),
(4, 'Computer Science', NULL, NULL),
(5, 'Genetic Engineering', NULL, NULL),
(6, 'Artificial Intelligence', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 13, 1, 'ddddd', '2020-03-18 04:23:43', '2020-03-18 04:23:43'),
(2, 13, 1, 'ddddd', '2020-03-18 04:24:30', '2020-03-18 04:24:30'),
(3, 47, 1, 'ehdjdsj', '2020-03-18 04:25:40', '2020-03-18 04:25:40'),
(4, 47, 1, 'krfhkfdhfdhd', '2020-03-18 04:44:16', '2020-03-18 04:44:16'),
(5, 13, 1, '3rd comment', '2020-03-18 04:44:42', '2020-03-18 04:44:42'),
(6, 13, 1, 'cjhdhhjdfhfhd', '2020-03-18 04:50:21', '2020-03-18 04:50:21'),
(7, 13, 1, 'hjhjv', '2020-03-18 04:50:43', '2020-03-18 04:50:43'),
(8, 13, 1, 'hgh', '2020-03-18 05:08:32', '2020-03-18 05:08:32'),
(9, 13, 1, 'jdsjshsd', '2020-03-18 05:20:57', '2020-03-18 05:20:57'),
(10, 13, 1, 'hi iam testing this comment section', '2020-03-18 05:21:32', '2020-03-18 05:21:32'),
(11, 13, 1, 'ffhhffh', '2020-03-18 05:41:15', '2020-03-18 05:41:15'),
(12, 13, 1, 'hoga', '2020-03-18 05:42:01', '2020-03-18 05:42:01'),
(13, 49, 1, 'asjasjdasgj', '2020-03-18 05:48:12', '2020-03-18 05:48:12'),
(14, 39, 3, 'nice post', '2020-03-18 11:48:05', '2020-03-18 11:48:05'),
(15, 39, 3, 'hello', '2020-03-18 11:51:28', '2020-03-18 11:51:28'),
(16, 49, 3, 'nice post', '2020-03-18 11:55:41', '2020-03-18 11:55:41'),
(17, 49, 2, 'hello', '2020-03-18 11:56:43', '2020-03-18 11:56:43'),
(18, 39, 1, 'good', '2020-03-18 12:10:19', '2020-03-18 12:10:19'),
(19, 66, 1, 'funny', '2020-03-19 00:19:11', '2020-03-19 00:19:11'),
(20, 50, 1, 'ddf', '2020-03-19 02:33:57', '2020-03-19 02:33:57'),
(21, 51, 1, 'wow , that\'s amassing', '2020-03-22 00:13:23', '2020-03-22 00:13:23');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sex` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `sex`, `created_at`, `updated_at`) VALUES
(1, 'Male', NULL, NULL),
(2, 'Female', NULL, NULL),
(3, 'Others', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `created_at`, `updated_at`, `user_id`, `post_id`) VALUES
(52, '2020-03-17 12:19:14', '2020-03-17 12:19:14', 1, 16),
(53, '2020-03-17 12:21:09', '2020-03-17 12:21:09', 1, 13),
(54, '2020-03-17 23:07:17', '2020-03-17 23:07:17', 1, 39),
(55, '2020-03-17 23:53:32', '2020-03-17 23:53:32', 3, 39),
(56, '2020-03-18 00:18:47', '2020-03-18 00:18:47', 1, 46),
(57, '2020-03-18 04:42:58', '2020-03-18 04:42:58', 1, 47),
(58, '2020-03-18 05:45:08', '2020-03-18 05:45:08', 1, 49),
(59, '2020-03-19 00:17:50', '2020-03-19 00:17:50', 2, 55),
(60, '2020-03-19 00:17:53', '2020-03-19 00:17:53', 2, 54),
(61, '2020-03-19 00:17:56', '2020-03-19 00:17:56', 2, 57),
(62, '2020-03-19 00:17:58', '2020-03-19 00:17:58', 2, 56),
(63, '2020-03-19 00:18:03', '2020-03-19 00:18:03', 2, 51),
(64, '2020-03-19 00:18:05', '2020-03-19 00:18:05', 2, 50),
(65, '2020-03-19 00:18:08', '2020-03-19 00:18:08', 2, 53),
(66, '2020-03-19 00:18:17', '2020-03-19 00:18:17', 2, 58),
(70, '2020-03-19 00:19:40', '2020-03-19 00:19:40', 1, 54),
(90, '2020-03-22 01:26:42', '2020-03-22 01:26:42', 1, 53),
(91, '2020-03-22 01:26:46', '2020-03-22 01:26:46', 1, 52);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\User', 1, 'default', 'shutterstock_1194075886', 'shutterstock_1194075886.jpg', 'image/jpeg', 'public', 169020, '[]', '[]', '[]', 1, '2020-03-11 13:20:17', '2020-03-11 13:20:17'),
(2, 'App\\User', 1, 'default', 'doctor_strange', 'doctor_strange.jpg', 'image/jpeg', 'public', 127007, '[]', '[]', '[]', 2, '2020-03-11 13:26:06', '2020-03-11 13:26:06'),
(45, 'App\\Article', 51, 'default', 'Moguls', 'Moguls.jpg', 'image/jpeg', 'public', 230439, '[]', '[]', '[]', 15, '2020-03-18 23:14:09', '2020-03-18 23:14:09'),
(46, 'App\\Article', 52, 'default', '9gfmm', '9gfmm.jpg', 'image/jpeg', 'public', 73136, '[]', '[]', '[]', 16, '2020-03-18 23:19:44', '2020-03-18 23:19:44'),
(47, 'App\\Article', 53, 'default', '1_kTtXWUzz9i9PzpFP4DBBWQ', '1_kTtXWUzz9i9PzpFP4DBBWQ.jpeg', 'image/jpeg', 'public', 94670, '[]', '[]', '[]', 17, '2020-03-18 23:21:45', '2020-03-18 23:21:45'),
(48, 'App\\Article', 54, 'default', 'Automotive-Manufacturing-cobots', 'Automotive-Manufacturing-cobots.jpg', 'image/jpeg', 'public', 76331, '[]', '[]', '[]', 18, '2020-03-18 23:25:03', '2020-03-18 23:25:03'),
(49, 'App\\Article', 55, 'default', 'CON12.19.', 'CON12.19..jpg', 'image/jpeg', 'public', 603168, '[]', '[]', '[]', 19, '2020-03-18 23:26:33', '2020-03-18 23:26:33'),
(50, 'App\\Article', 56, 'default', 'A_Covariant_robot_at_a_KNAPP_powered_warehouse_Obeta_outside_Berlin_.0', 'A_Covariant_robot_at_a_KNAPP_powered_warehouse_Obeta_outside_Berlin_.0.jpg', 'image/jpeg', 'public', 179971, '[]', '[]', '[]', 20, '2020-03-18 23:28:38', '2020-03-18 23:28:38'),
(51, 'App\\Article', 57, 'default', 'RIA-SR-MAR2020-T1-1060861908-SM-TWT', 'RIA-SR-MAR2020-T1-1060861908-SM-TWT.jpg', 'image/jpeg', 'public', 387493, '[]', '[]', '[]', 21, '2020-03-18 23:32:45', '2020-03-18 23:32:45'),
(52, 'App\\Article', 58, 'default', 'A_Horseshoe_Einstein_Ring_from_Hubble', 'A_Horseshoe_Einstein_Ring_from_Hubble.jfif', 'image/jpeg', 'public', 111366, '[]', '[]', '[]', 22, '2020-03-18 23:52:00', '2020-03-18 23:52:00'),
(53, 'App\\Article', 59, 'default', 'WaPcAbmKUubFWkFcbb8WYe-1200-80', 'WaPcAbmKUubFWkFcbb8WYe-1200-80.jpg', 'image/jpeg', 'public', 135505, '[]', '[]', '[]', 23, '2020-03-18 23:52:56', '2020-03-18 23:52:56'),
(54, 'App\\Article', 60, 'default', 'Leo-I-trio-480x274-736x490-c-default', 'Leo-I-trio-480x274-736x490-c-default.jpg', 'image/jpeg', 'public', 45044, '[]', '[]', '[]', 24, '2020-03-18 23:54:57', '2020-03-18 23:54:57'),
(55, 'App\\Article', 61, 'default', 'world_models_1990', 'world_models_1990.jpeg', 'image/jpeg', 'public', 140163, '[]', '[]', '[]', 25, '2020-03-18 23:59:47', '2020-03-18 23:59:47'),
(56, 'App\\Article', 62, 'default', '1_dOv2a1ctNrHDo8Zks30Bbw', '1_dOv2a1ctNrHDo8Zks30Bbw.png', 'image/png', 'public', 350387, '[]', '[]', '[]', 26, '2020-03-19 00:01:45', '2020-03-19 00:01:45'),
(57, 'App\\Article', 63, 'default', 'CSCI1430_EyeRobot_Credited_2020', 'CSCI1430_EyeRobot_Credited_2020.png', 'image/png', 'public', 440984, '[]', '[]', '[]', 27, '2020-03-19 00:03:06', '2020-03-19 00:03:06'),
(58, 'App\\Article', 64, 'default', '20191002-sickle-cell', '20191002-sickle-cell.jpg', 'image/jpeg', 'public', 43358, '[]', '[]', '[]', 28, '2020-03-19 00:07:59', '2020-03-19 00:07:59'),
(59, 'App\\Article', 65, 'default', 'melanoma_db_gallerydl', 'melanoma_db_gallerydl.png', 'image/png', 'public', 969376, '[]', '[]', '[]', 29, '2020-03-19 00:10:08', '2020-03-19 00:10:08'),
(60, 'App\\Article', 66, 'default', 'donald-trump-ai-initiative.001', 'donald-trump-ai-initiative.001.jpeg', 'image/jpeg', 'public', 118499, '[]', '[]', '[]', 30, '2020-03-19 00:11:32', '2020-03-19 00:11:32'),
(61, 'App\\Article', 67, 'default', 'A_Covariant_robot_at_a_KNAPP_powered_warehouse_Obeta_outside_Berlin_.0', 'A_Covariant_robot_at_a_KNAPP_powered_warehouse_Obeta_outside_Berlin_.0.jpg', 'image/jpeg', 'public', 179971, '[]', '[]', '[]', 31, '2020-03-19 00:15:21', '2020-03-19 00:15:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_03_08_063612_post', 2),
(5, '2020_03_08_064359_post_type', 2),
(6, '2020_03_08_064831_blood_group', 2),
(7, '2020_03_08_080728_create_posts_table', 3),
(8, '2020_03_08_080836_create_post_types_table', 3),
(9, '2020_03_08_080923_create_blood_groups_table', 3),
(10, '2020_03_09_081755_create_genders_table', 4),
(11, '2020_03_09_114126_create_likes_table', 5),
(13, '2020_03_11_054054_create_articles_table', 6),
(14, '2020_03_11_184725_create_media_table', 7),
(15, '2020_03_12_113845_create_categories_table', 8),
(16, '2020_03_16_065845_add_like_count_to_articles_table', 9),
(17, '2020_03_17_184918_create_permission_tables', 10),
(18, '2020_03_18_072819_create_comments_table', 11),
(19, '2020_03_22_094631_add_softdelete_to_users', 12),
(20, '2020_03_22_095220_add_softdelete_to_articles', 13);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Gender` tinyint(4) DEFAULT NULL,
  `bloodgroup_id` tinyint(4) DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `Gender`, `bloodgroup_id`, `address`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Faisal', 'faisal@gmail.com', NULL, '$2y$10$Q.bF4Php6XmKZgHBiBk9t.kwqk1rXMkKgAZQvFW9epewB9cirTs6S', 1, 6, 'sssssss', NULL, '2020-03-08 00:20:31', '2020-03-11 00:02:07', NULL),
(2, 'Musa', 'musa@m.com', NULL, '$2y$10$uWoz8B0I/aoVtKgo3KwXwOKVZqmZNahst0UKXQZjKmU/C1scAKG2e', NULL, 1, 'xxxx', NULL, '2020-03-11 00:43:54', '2020-03-14 13:21:18', NULL),
(3, 'pial', 'pial@p.com', NULL, '$2y$10$6oUosB30804oBxDJvkN2D.yWoWYeJo5rV36bYW5ej8/eMt.zzvVc2', NULL, 3, 'mohanogor', NULL, '2020-03-17 23:53:23', '2020-03-19 00:25:58', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blood_groups`
--
ALTER TABLE `blood_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `blood_groups`
--
ALTER TABLE `blood_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
