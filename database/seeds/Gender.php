<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class Gender extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['sex'=> 'Male'],
            ['sex'=> 'Female'],
            ['sex'=> 'Others'],
            ['sex'=> 'Computer Science'],
            ['sex'=> 'Genetic Engineering'],
            ['sex'=> 'Artificial Intelligence'],
        ];

        DB::table('genders')->insert(
            $data
        );
    }
}
