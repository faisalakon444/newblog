<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['catagory_name'=> 'Physics'],
            ['catagory_name'=> 'Robotics'],
            ['catagory_name'=> 'Astronomy'],
            ['catagory_name'=> 'Computer Science'],
            ['catagory_name'=> 'Genetic Engineering'],
            ['catagory_name'=> 'Artificial Intelligence'],
        ];

        DB::table('categories')->insert(
            $data
        );
    }
}
