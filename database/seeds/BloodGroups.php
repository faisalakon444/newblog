<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class BloodGroups extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   
    public function run()
    {
        $data = [
            ['groups'=> 'A+'],
            ['groups'=> 'A-'],
            ['groups'=> 'B+'],
            ['groups'=> 'B-'],
            ['groups'=> 'AB+'],
            ['groups'=> 'AB-'],
            ['groups'=> 'O+'],
            ['groups'=> 'O-'],
        ];

        DB::table('blood_groups')->insert(
            $data
        );
    }
}
