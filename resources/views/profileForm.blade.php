

@extends('master')

@section('content')

<div class="container">
    <div class="contact-form bottom">
      <h2 class="page-header" style="font-weight: bold;color: rgb(79, 204, 205)">So you want to update your profile</h2>
        {{-- <h2 style="color:red">So you want to update your profile</h2> --}}
        <form method="post" action="{{ route('profile.update') }}">
            {{ csrf_field() }}
            <div class="form-group input-group">
                <span class="input-group-addon">User Name</span>
            <input type="text" name="name" class="form-control" value="{{$userData['name']}}">
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon">Email</span>
                <input type="text" name="email" class="form-control" value="{{$userData['email']}}">
            </div>
           
            @if($gender ?? "")
            <div class="form-group input-group">
               <span class="input-group-addon">Gender</span>
               <select class="form-control" name="bloodgroup_id">
                   @if ($userData['Gender'])

                   @foreach($gender as $postType)
                 @if ($userData['Gender']==$postType['id'])
               <option selected disabled>{{$postType['sex']}}</option>
                 @endif
                 @endforeach
                       
                   @else
                   <option selected disabled>Please Select</option>
                   @endif
                 
       
                 @foreach($gender as $postType)
                 <option value="{{$postType->id}}">{{$postType->sex}}</option>
                 @endforeach
               </select>
            
             </div>
             @endif
              @if($bloods ?? "")
             <div class="form-group input-group">
                <span class="input-group-addon">Blood Groups</span>
                <select class="form-control" name="bloodgroup_id">
                    @if ($userData['bloodgroup_id'])

                    @foreach($bloods as $postType)
                  @if ($userData['bloodgroup_id']==$postType['id'])
                <option selected disabled>{{$postType['groups']}}</option>
                  @endif
                  @endforeach
                        
                    @else
                    <option selected disabled>Please Select</option>
                    @endif
                  
        
                  @foreach($bloods as $postType)
                  <option value="{{$postType->id}}">{{$postType->groups}}</option>
                  @endforeach
                </select>
             
              </div>
              @endif
            <div class="form-group input-group">
                <span class="input-group-addon">Address</span>
                <textarea name="address" class="form-control" rows="2">{{$userData['address']}}</textarea>
            </div>                        
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-submit" value="Update">
            </div>
        </form>
    </div>
</div>
    @endsection
    