<div class="col-md-3 col-sm-5">
    <div class="sidebar blog-sidebar">
        {{-- <div class="sidebar-item  recent">
            <h3>Comments</h3>
            <div class="media">
                <div class="pull-left">
                    <a href="#"><img src="{{asset('images/portfolio/project1.jpg')}}" alt=""></a>
                </div>
                <div class="media-body">
                    <h4><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit,</a></h4>
                    <p>posted on  07 March 2014</p>
                </div>
            </div>
            <div class="media">
                <div class="pull-left">
                    <a href="#"><img src="{{asset('images/portfolio/project2.jpg')}}" alt=""></a>
                </div>
                <div class="media-body">
                    <h4><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit,</a></h4>
                    <p>posted on  07 March 2014</p>
                </div>
            </div>
            <div class="media">
                <div class="pull-left">
                    <a href="#"><img src="{{asset('images/portfolio/project3.jpg')}}" alt=""></a>
                </div>
                <div class="media-body">
                    <h4><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit,</a></h4>
                    <p>posted on  07 March 2014</p>
                </div>
            </div>
        </div> --}}
        <div class="sidebar-item categories">
            <h3>Categories</h3>
            <ul class="nav navbar-stacked">
                @foreach ($sections as $section)
                
                
                <li ><a href="{{url('/section'.'/'.$section['id'])}}">{{$section['catagory_name']}}<span class="pull-right">({{$section->articleCount}})</span></a>
                </li>  
                @endforeach
            </ul>
        </div>
        {{-- <div class="sidebar-item tag-cloud">
            <h3>Tag Cloud</h3>
            <ul class="nav nav-pills">
                <li><a href="#">Corporate</a></li>
                <li><a href="#">Joomla</a></li>
                <li><a href="#">Abstract</a></li>
                <li><a href="#">Creative</a></li>
                <li><a href="#">Business</a></li>
                <li><a href="#">Product</a></li>
            </ul>
        </div>
        <div class="sidebar-item popular">
            <h3>Latest Photos</h3>
            <ul class="gallery">
                <li><a href="#"><img src="{{asset('images/portfolio/popular1.jpg')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('images/portfolio/popular2.jpg')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('images/portfolio/popular3.jpg')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('images/portfolio/popular4.jpg')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('images/portfolio/popular5.jpg')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('images/portfolio/popular1.jpg')}}" alt=""></a></li>
            </ul>
        </div> --}}
    </div>
</div>