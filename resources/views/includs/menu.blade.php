<header id="header">      
    <div class="container">
        <div class="row">
            <div class="col-sm-12 overflow">
               {{-- <div class="social-icons pull-right">
                    <ul class="nav nav-pills">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                        <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>  --}}
            </div>
         </div>
    </div>
    <div class="navbar navbar-inverse" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                @if (Auth::guest())
                <a class="navbar-brand" href="{{url('/')}}">
                    <h1><img src="{{asset('images/logo.png')}}" alt="logo"></h1>
                </a> 
                @else
                <a class="navbar-brand" href="{{route('home')}}">
                    <h1><img src="{{asset('images/logo.png')}}" alt="logo"></h1>
                </a> 
                @endif
            
                
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                    <li class=""><a href="{{ url('/') }}">Home</a></li> 
                    @else
                    <li class=""><a href="{{ route('home') }}">Home</a></li> 
                    <li ><a href="{{url('/allpost')}}">ALL-ARTICLES</a> 
                    @endif
                    

                    @if (Auth::user())
                    <li class="dropdown"><a>My Posts<i class="fa fa-angle-down"></i></a>
                        <ul role="menu" class="sub-menu">
                            
                            <li ><a href="{{ route('post.show') }}">Show My Articles</a>
                            </li>
                            <li ><a href="{{ route('post.form') }}">Create Article</a>
                            </li>
                        </ul>
                    </li> 
                    @endif
                    @if (Auth::user())
                    <li class="dropdown"><a>Sections<i class="fa fa-angle-down"></i></a>
                        <ul role="menu" class="sub-menu">
                            @foreach ($sections as $section)
                            <li ><a href="{{url('/section'.'/'.$section['id'])}}">{{$section['catagory_name']}}</a>
                            </li>  
                            @endforeach
                        
                            
                        </ul>
                    </li>
                    @endif
                <li ><a href="{{url('/about')}}">About</a>
                    </li>
                   
                                        
                    
                    @if (Auth::guest())
                    <li ><a href="{{ route('login') }}">Login</a>
                    <li ><a href="{{ route('register') }}">Registration</a>
                    @else
                    <li class="dropdown"><a>Account <i class="fa fa-angle-down"></i></a>
                        <ul role="menu" class="sub-menu">
                            
                            <li ><a href="{{url('/profile')}}">My Profile</a>
                            <li ><a href="{{ route('profile.form') }}">Profile Update</a>
                            <li ><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Logout</a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form> 
                                
                                </li>
                        </ul>
                    </li>
                      
                    @endif
                                 
                </ul>
            </div>
            <div class="search">
                <form role="form">
                    <i class="fa fa-search"></i>
                    <div class="field-toggle">
                        <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>
<!--/#header-->