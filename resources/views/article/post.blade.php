@extends('master')
@section('content')

<section id="blog-details" class="padding-top">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-7">
                <div class="row">
                     <div class="col-md-12 col-sm-12">
                        <div class="single-blog blog-details two-column">
                            <div class="post-thumb">
                                @foreach ($image as $im)
                                <a><img style="height:400px; width:850px" src="{{$im->getUrl()}}" class="img-responsive" alt=""></a>
                                @endforeach
                                
                                <div class="post-overlay">
                                    <span class="uppercase"><a>{{date_format($postData['created_at'], 'd')}}<br><small>{{date_format($postData['created_at'], 'M')}}</small></a></span>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                
                            <h2 class="post-title bold"><a>{{$postData['title']}}</a></h2>
                            @foreach ($userData as $item)
                                   @if ($item['id']==$postData['user_id'])
  
                                   <h3 class="post-author"><a>{{$item['name']}} || {{date_format($postData['created_at'], 'd - M - Y')}}</a></h3>
                                   @endif 
                            @endforeach
                                
                            <p>{{$postData['article_body']}}</p>
                                <div class="post-bottom overflow">
                                    <ul class="nav navbar-nav post-nav">
                                        <li>
                                           
                                           @if ($like>0)
                                           <a name="submit" id="like_button" class="btn" value=""  onclick="likes('{{$postData['id']}}',1)">{{$postData['like_count']}} <i class="fa fa-heart"></i>UnLike</a>
 
                                           @else
                                           <a name="submit" class="btn " value=""  onclick="likes('{{$postData['id']}}',2)">{{$postData['like_count']}} <i class="fa fa-heart"></i> Like</a>

                                           @endif

                                            
                                            </li>
                                        <li><a name="submit" onclick="comment_f()" class="btn" >{{$comments->count()}} <i class="fa fa-comments"></i>Comments</a></li>
                                            
                                        </ul>
                                        <div style="display:none; padding-top:30px;" id="comid" class="card" >
                                           
                                            <textarea style="border-color:black;" id="myText"  class="form-control"  name="comment" type="text"></textarea>
                                            <button onclick="comment(document.getElementById('myText').value,'{{$postData['id']}}', '{{Auth::user()->id}}')" name="submit" class="btn btn-submit" value="Submit"> submit</button>
                                        </div>
                                </div>
                               
                                
                                <div class="author-profile padding">
                                    
                                </div>
                                <div class="response-area">
                                <h2 class="bold">Comments</h2>
                                <ul class="media-list">
                                    <li class="media">
                                        @foreach ($comments as $comment)
                                            
                                       
                                        <div class="post-comment">
                                            <a class="pull-left" href="#">
                                                <img class="media-object" src="images/blogdetails/2.png" alt="">
                                            </a>
                                            <div class="media-body">
                                                @foreach ($userData as $item)
                                                @if ($item['id']==$comment['user_id'])
                                                <span><i class="fa fa-user"></i>Posted by <a>{{$item['name']}}</a></span>
                                  
  
                                                @endif 
                                                @endforeach
                                                
                                            <p>{{$comment->comment}}</p>
                                                <ul class="nav navbar-nav post-nav">
                                                <li><a><i class="fa fa-clock-o"></i>{{$comment->created_at}}</a></li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        @endforeach
                                        
                                    </li>
        
                                    
                                </ul>  
                                <div class="blog-pagination">
                                    <ul class="pagination">
                                    
                                    {{ $comments->links() }}
                                </ul>
                                   
                                </div>                 
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
           
            @include('includs.side')
        </div>
    </div>
</section>
<!--/#blog-->

<script>

function likes(i,a){
            if (a>1) {
                var j='/likes';
                console.log(a);
            } else {
                var j='/dislikes';
                console.log(a);

            }
            $.ajax({
            method: 'POST', 
            url: j, 
            data: {"_token": "{{ csrf_token() }}",'post_id':i}, 
            success: function() {   
                location.reload();  
            },
            error: function(jqXHR, textStatus, errorThrown) { 
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
        }


function comment_f() {
 
    $("#comid").toggle();

}
function comment(i,a,b){
    $.ajax({
    method: 'POST', // Type of response and matches what we said in the route
    url: '/comment', // This is the url we gave in the route
    data: {"_token": "{{ csrf_token() }}",'post_id':a,'user_id':b,'comment':i}, // a JSON object to send back
    success: function() {   
        location.reload();  
    },
    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
    }
});
   
}

</script>
@endsection







