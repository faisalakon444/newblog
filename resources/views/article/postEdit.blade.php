

@extends('master')

@section('content')

<div class="container">
    <div class="contact-form bottom">
        <div class="post-thumb form-group">
            @foreach ($image as $im)
            <a ><img style="height:200px; width:450px" src="{{$im->getUrl()}}" class="img-responsive" alt=""></a>
            @endforeach
            <br>
            
            <div class="post-overlay">
                <span class="uppercase"><a href="#">14 <br><small>Feb</small></a></span>
            </div>
        </div>
        <h2 style="font-weight: bold;color: rgb(79, 204, 205)">Update Your Article</h2>
        <form method="post" action="{{ route('post.update') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
           
            <input hidden name="id" type="number" value="{{$postData['id']}}">
            <div class="form-group input-group">
                <span class="input-group-addon">Image</span>
                <input type="file" name="image" class="form-control" placeholder="">
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon">Title</span>
                <input type="text" name="title" class="form-control" required="required" value="{{$postData['title']}}">
            </div>
             @if($category)
             <div class="form-group input-group">
                <span class="input-group-addon">Section</span>
                <select class="form-control" name="article_type">
                  <option selected disabled>Please Select</option>
        
                  @foreach($category as $category)
                  <option value="{{$category->id}}">{{$category ->catagory_name}}</option>
                  @endforeach
                </select>
             
              </div>
              @endif
            <div class="form-group input-group">
                <span class="input-group-addon">Body</span>
                <textarea name="article_body" required="required" class="form-control" rows="4" placeholder="Your text here">{{$postData['article_body']}}</textarea>
            </div>                        
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-submit" value="Submit">
            </div>
        </form>
    </div>
</div>
    @endsection
    