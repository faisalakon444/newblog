

@extends('master')

@section('content')

<div class="container">
    <div class="contact-form bottom">
        <h2 style="font-weight: bold;color: rgb(79, 204, 205)">Make A Nice post</h2>
        <form method="post" action="{{ route('post.save') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group input-group">
                <span class="input-group-addon">Title</span>
                <input type="text" name="title" class="form-control" required="required" placeholder="Title">
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon">Image</span>
                <input type="file" name="image" class="form-control" required="required" placeholder="Title">
            </div>
           
             @if($category)
             <div class="form-group input-group">
                <span class="input-group-addon">Section</span>
                <select class="form-control" name="post_type">
                  <option selected disabled>Please Select</option>
        
                  @foreach($category as $category)
                  <option value="{{$category->id}}">{{$category ->catagory_name}}</option>
                  @endforeach
                </select>
             
              </div>
              @endif
            <div class="form-group input-group">
                <span class="input-group-addon">Section</span>
                <textarea name="post_body" required="required" class="form-control" rows="4" placeholder="Your text here"></textarea>
            </div>                        
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-submit" value="Submit">
            </div>
        </form>
    </div>
</div>
    @endsection
    