

@extends('master')

@section('content')
@if ($catagory ?? '' )

<section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                    <h1 class="title">{{$catagory->catagory_name}}</h1>
                    <p style="color:#C03035">{{$catagory->catagory_name}} category has {{$posts->count()}} articles</p>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</section>
     
 @else
 <section id="page-breadcrumb">
    <div class="vertical-center sun">
         <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                    <h1 class="title">Articles</h1>
                    <p style="color:#C03035">Total Articles {{$posts->count()}}</p>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</section> 
 @endif   

<section id="blog" class="padding-top">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-7">
                <div class="row">
                    @if ($posts ?? '')
                    @foreach($posts ?? '' ?? '' as $post )
                   
                        
                    
                     <div class="col-md-6 col-sm-12 blog-padding-right">
                        <div class="single-blog two-column">
                            <div class="post-thumb">
                                @foreach ($post['media'] as $im)
                                <a><img style="height:270px; width:480px" src="{{$im->getUrl()}}" class="img-responsive" alt=""></a>
                                @endforeach                                
                                <div class="post-overlay">
                                    <span class="uppercase"><a>{{date_format($post['created_at'], 'd')}}<br><small>{{date_format($post['created_at'], 'M')}}</small></a></span>
                                </div>
                            </div>
                            <div class="post-content overflow">
                            <h2 class="post-title bold"><a>{{$post->title}}</a></h2>
                           
                            <h3 class="post-author"><a>Posted by {{$post['user']['name']}}</a></h3>
                                <p>{{ substr(strip_tags($post->article_body), 0, 300) }}</p>
                                <div style="display:flex">
                                    <a type="button" style="margin:0 10px 0 0px" class="btn btn-info" href="{{url('/post'.'/'.$post['id'])}}">view</a>
                                    
                                    </div>
                                <div class="post-bottom overflow">
                                    <ul class="nav post-nav">
                                       
                                        
                                       
                                        <li>
                                            <li>
                                                @if ($post->isLiked>0)
                                                <a name="submit" id="like_button" class="btn" value=""  onclick="likes('{{$post['id']}}',1)">{{$post['like_count']}} <i class="fa fa-heart"></i>UnLike</a>
        
                                                @else
                                                <a name="submit" class="btn " value=""  onclick="likes('{{$post['id']}}',2)">{{$post['like_count']}} <i class="fa fa-heart"></i>Like</a>

                                                @endif
                                                
                            
                                                 </li>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    @endforeach
                    @endif
                </div>
                <div> </div>
               
                <div class="blog-pagination">
                    <ul class="pagination">
                   
                    {{ $posts->links() }}
                </ul>
                   
                </div>
             </div>
             @include('includs.side')
        </div>
    </div>
</section>

<script>
    function likes(i,a){
            if (a>1) {
                var j='/likes';
                console.log(a);
            } else {
                var j='/dislikes';
                console.log(a);

            }
            $.ajax({
            method: 'POST', 
            url: j, 
            data: {"_token": "{{ csrf_token() }}",'post_id':i}, 
            success: function() {   
                location.reload();  
            },
            error: function(jqXHR, textStatus, errorThrown) { 
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
        }
    
    </script>

@endsection


    