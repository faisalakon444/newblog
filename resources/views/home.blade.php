

@extends('master')

@section('content')

    <section id="home-slider">
        <div class="container">
            <div class="row">
                <div class="main-slider">
                    <div class="slide-text">
                        <h2>{{$popularArticles{0}->title}}</h2>
                      <p>{{ substr(strip_tags($popularArticles{0}->article_body), 0, 300) }}</p>
                        {{-- <img src="smiley.gif" alt="Smiley face" width="42" height="42"> --}}
                        <p></p>
                    <a href="{{url('/post'.'/'.$popularArticles{0}['id'])}}" class="btn btn-common">Read More</a>
                    </div>
                    <img src="{{asset('images/home/slider/hill.png')}}" class="slider-hill" alt="slider image">
                    <img src="{{asset('images/home/slider/house.png')}}" class="slider-house" alt="slider image">
                    <img src="{{asset('images/home/slider/sun.png')}}" class="slider-sun" alt="slider image">
                    <img src="{{asset('images/home/slider/birds1.png')}}" class="slider-birds1" alt="slider image">
                    <img src="{{asset('images/home/slider/birds2.png')}}" class="slider-birds2" alt="slider image">
                </div>
            </div>
        </div>
        <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
        <br><br>
    </section>
    <section id="team">
        <div class="container">
            <div class="row">
                <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Recent Articles</h1>
                <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>
                Ut enim ad minim veniam, quis nostrud </p>
                <div id="team-carousel" class="carousel slide wow fadeIn" data-ride="carousel" data-wow-duration="400ms" data-wow-delay="400ms">
                    <!-- Indicators -->
                    <ol class="carousel-indicators visible-xs">
                        <li data-target="#team-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#team-carousel" data-slide-to="1"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            @foreach ($articles as $item)
                            <div class="col-sm-3 col-xs-6">
                               
                                    
                            <a href="{{url('/post'.'/'.$item['id'])}}">
                                <div class="team-single-wrapper">
                                    <div class="team-single">
                                        <div class="person-thumb">
                                            @foreach ($item['media'] as $im)
                                            <img style="height:270px; width:480px" src="{{$im->getUrl()}}" class="img-responsive" alt="">
                                            @endforeach  
                                        </div>
                                        
                                    </div>
                                    <div class="person-info">
                                        <h2>{{$item->title}}</h2>
                                @foreach ($userData as $user)
                                   @if ($user['id']==$item['user_id'])
  
                                   <h3 class="post-author">{{$user['name']}} || {{date_format($item['created_at'], 'd - M - Y')}}</h3>
                                   @endif 
                                @endforeach
                                    </div>
                                </div>
                            </a>
                            </div>
                             @endforeach
                        </div>
                        
                        <div class="item">
                            @foreach ($articles as $item)
                            <div class="col-sm-3 col-xs-6">
                               
                                    
                                <a href="{{url('/post'.'/'.$item['id'])}}">
                                <div class="team-single-wrapper">
                                    <div class="team-single">
                                        <div class="person-thumb">
                                            @foreach ($item['media'] as $im)
                                            <img style="height:270px; width:480px" src="{{$im->getUrl()}}" class="img-responsive" alt="">
                                            @endforeach  
                                        </div>
                                        
                                    </div>
                                    <div class="person-info">
                                    <h2>{{$item->title}}</h2>
                                @foreach ($userData as $user)
                                   @if ($user['id']==$item['user_id'])
  
                                   <h3 class="post-author">{{$user['name']}} || {{date_format($item['created_at'], 'd - M - Y')}}</h3>
                                   @endif 
                                @endforeach
                                    </div>
                                </div>
                                </a>
                            </div>
                             @endforeach
                            
                            
                           
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left team-carousel-control hidden-xs" href="#team-carousel" data-slide="prev">left</a>
                    <a class="right team-carousel-control hidden-xs" href="#team-carousel" data-slide="next">right</a>
                </div>
            </div>
        </div>
    </section>
    <section id="features">
        <div class="container">
            <div class="row">
                
                <section id="page-breadcrumb">
                    <div class="vertical-center sun">
                         <div class="container">
                            <div class="row">
                                <div style="text-align:center" class="action">
                                    <div class="col-sm-12">
                                    <h1 class="title">PopularArticles</h1>
                                        <p>People like to read these articles</p>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </section> 
                    
                
                <div class="single-features">
                    <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                        @foreach ($popularArticles{0}->media as $item)
                        <a href="{{url('/post'.'/'.$popularArticles{0}['id'])}}">
                            <img style="width:427px; height:140px" src="{{$item->getUrl()}}" class="img-responsive" alt="">
                        </a>
                        @endforeach
                        
                    </div>
                    <div class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                        <a href="{{url('/post'.'/'.$popularArticles{0}['id'])}}"><h2>{{$popularArticles{0}->title}}</h2></a>
                    @foreach ($userData as $user)
                                   @if ($user['id']==$popularArticles{0}['user_id'])
  
                                   <h3 class="post-author">{{$user['name']}} || {{date_format($popularArticles{0}['created_at'], 'd - M - Y')}}</h3>
                                   @endif 
                    @endforeach
                      <p>{{ substr(strip_tags($popularArticles{0}->article_body), 0, 150).'..........' }}</p>  
                </div>
                </div>
                <div class="single-features">
                    <div class="col-sm-6 col-sm-offset-1 align-right wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                        <a href="{{url('/post'.'/'.$popularArticles{1}['id'])}}"><h2>{{$popularArticles{1}->title}}</h2></a>
                        @foreach ($userData as $user)
                                   @if ($user['id']==$popularArticles{1}['user_id'])
  
                                   <h3 class="post-author">{{$user['name']}} || {{date_format($popularArticles{1}['created_at'], 'd - M - Y')}}</h3>
                                   @endif 
                    @endforeach
                      <p>{{ substr(strip_tags($popularArticles{1}->article_body), 0, 150) }}</p>
                     </div>
                    <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                        @foreach ($popularArticles{1}->media as $item)
                        <a href="{{url('/post'.'/'.$popularArticles{1}['id'])}}">
                        <img style="width:427px; height:140px" src="{{$item->getUrl()}}" class="img-responsive" alt="">
                        </a>
                        @endforeach
                    </div>
                </div>
                <div class="single-features">
                    <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                        @foreach ($popularArticles{2}->media as $item)
                        <a href="{{url('/post'.'/'.$popularArticles{2}['id'])}}">
                        <img style="width:427px; height:140px" src="{{$item->getUrl()}}" class="img-responsive" alt="">
                        </a>
                        @endforeach
                    </div>
                    <div class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                        <a href="{{url('/post'.'/'.$popularArticles{2}['id'])}}"><h2>{{$popularArticles{2}->title}}</h2></a>
                        @foreach ($userData as $user)
                                   @if ($user['id']==$popularArticles{2}['user_id'])
  
                                   <h3 class="post-author">{{$user['name']}} || {{date_format($popularArticles{2}['created_at'], 'd - M - Y')}}</h3>
                                   @endif 
                    @endforeach
                      <p>{{ substr(strip_tags($popularArticles{2}->article_body), 0, 150) }}</p>
                     </div>
                </div>
            </div>
        </div>
    </section>
    @endsection
    