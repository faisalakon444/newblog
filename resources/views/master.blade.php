@include('includs.head')
<body>
	@include('includs.menu')

    @yield('content')
    @yield('post')

    @include('includs.footer')

