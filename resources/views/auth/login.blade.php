

@extends('master')

@section('content')

<div class="container">
    <div class="contact-form bottom">
        <h2 style="font-weight: bold;color: rgb(79, 204, 205)">Login Form</h2>
        <form method="post" action="{{ route('login') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group input-group">
                <span class="input-group-addon">Email</span>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon">Password</span>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

            @error('password')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
           
           
             <div class="form-group input-group">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
             
              </div>
                                
            <div class="form-group">
                <button type="submit" class="btn btn-submit">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
                
            </div>
        </form>
    </div>
</div>
    @endsection
    
