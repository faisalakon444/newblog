

@extends('master')

@section('content')

<div class="container">
    <div class="contact-form bottom">
        <h2 style="font-weight: bold;color: rgb(79, 204, 205)">Registration Form</h2>
        <form method="post" action="{{ route('register') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group input-group">
                <span class="input-group-addon">Name</span>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon">Email</span>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon">Password</span>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon">Confirm Password</span>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

            </div>
           
           
            
                                
            <div class="form-group">
                <button type="submit" class="btn btn-submit">
                    {{ __('Register') }}
                </button>
                
            </div>
        </form>
    </div>
</div>
    @endsection
    
