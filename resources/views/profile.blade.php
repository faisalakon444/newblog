@extends('master')

@section('content')
    <section id="page-breadcrumb">
        <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">Portfolio</h1>
                            <p>Be Creative</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->

    <section id="portfolio-information" class="padding-top">
        <div class="container">
            <div class="container">
            <div class="row">
                <div class="col-sm-2">
                   
                </div>
                
                <div class="col-sm-4">
                    <img style="weight:300px; height:300px" src="images/portfolio-details/2.jpg" class="img-responsive" alt="">
                </div>
                <div class="col-sm-4">
                    <div class="project-name overflow">
                        <h2 class="bold">{{$userData['name']}}</h2>
                        <ul class="nav navbar-nav navbar-default">
                            <li>Join Date <i class="fa fa-clock-o"></i> <br>{{$userData['created_at']}}</li>
                        </ul>
                    </div>
                    
                    <div class="skills overflow">
                        <h3>Email:</h3>
                        <ul class="nav navbar-nav navbar-default">
                            <li><i class="fa fa-envelope"></i> {{$userData['email']}}</li>
                            
                        </ul>
                    </div>
                    <div class="client overflow">
                        <h3>Address:</h3>
                        <ul class="nav navbar-nav navbar-default">
                            <li><i class="fa fa-map-marker"></i> {{$userData['address']}}</li>
                        </ul>
                    </div>
                    <div class="client overflow">
                        <h3>Blood Group:</h3>
                        @if (isset($userData['bloodgroup_id']))
                           @foreach ($bloods as $item)
                           @if ($userData['bloodgroup_id']==$item['id'])
                           <ul class="nav navbar-nav navbar-default">
                            <li><i style="color:red" class="fa fa-heartbeat"></i> {{$item['groups']}}</li>
                          </ul>
                           @endif
                           @endforeach 
                        @endif
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
               
            </div>
        </div>
    </div>
    </section>
     <!--/#portfolio-information-->

    <section id="related-work" class="padding-top padding-bottom">
        <div class="container">
            <div class="row">
                <h1 class="title text-center">Your Recent Articles</h1>
                @foreach ($articles as $article)
                <a href="{{url('/post'.'/'.$article['id'])}}">
                <div class="col-sm-3">
                   
                    <div class="portfolio-wrapper">
                        <div class="portfolio-single">
                            <div class="portfolio-thumb">
                                @foreach ($article['media'] as $img)
                                <img style="height:261px; width:269px;" src="{{$img->getUrl()}}" class="img-responsive" alt="">  
                                @endforeach
                                
                            </div>
                        </div>
                        <div class="portfolio-info ">
                        <h2>{{$article->title}}</h2>
                        </div>
                    </div>
                </div>
                </a>
                @endforeach
                
            </div>
        </div>
    </section>
    <!--/#related-work-->


@endsection
